# Rules / Terms of Service

## We do not allow:

 - Spoiling of somewhat recent movies, games, books, etc. Use the content warning (If you're using the Mastodon UI)
 - Pornographic avatars (No I won't complain about half-naked anime girls, just keep nipples and genitals away)
 - Untagged NSFW
 - Continued, intentional and targeted bullying/spamming.
 - Exiling/Threatening someone for their opinion. You're not obligated to listen, nor to care, but they still have the right to say what they want as long as their actions don't break any other rules. If you don't like someone, you block them and call it a day
 - Nothing that's actually illegal. Like child pornography or unironic bomb threats

## We do allow:

 - Controversial opinions
 - "Questionable jokes"
 - Shitposting (But please stop spamming people if they ask. They should not have to ask multiple times)
 - Most other things if not mentioned in the list of things we don't allow, just don't be an asshole

## Additional information:

 - We do not use instance-wide blocks unless it's to combat spambots. Each user is responsible for their own timeline. I am responsible for the local timeline. And the federated timeline is a free-for-all.
 - The rule about targeting someone for their opinion may be overruled where someone's opinion is central. e.g while discussing presidential elections, since their personalities is most of the discussion material. Or while drama arises on subjects such as the mental state of a school shooter. In other words: If a bunch of news sites around the world are already complaining about it, it's probably okay for you to complain too.
 - For now we are invite only. That means we currently host no bots (and probably never will)
 - No. We're not an "alt-right" instance. Free speech does not mean we're nazis. But it does mean people on this instance are not obligated to pretend that they agree with you. "If you can't take the heat, get out of the kitchen"

## Instance description:

This is an instance focused on general discussion, but often anime or tech. Any subject is allowed on this instance as long as you don't break any of the rules. We support free speech as long as it does not elevate into direct threats.

We understand some might find this hard to moderate. But if you're an admin who absolutely must block for some reason. I urge you to at least block individual users and not the whole instance.